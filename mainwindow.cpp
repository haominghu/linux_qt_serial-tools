#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    int count = 0;
    //设置控件初始状态
    ui->label_recvbyte_count->setText("0");
    ui->label_sendbyte_count->setText("0");
    ui->label_state->setStyleSheet(this->state_grey_SheetStyle);
    //设置标志位初始状态
    this->pushButton_open_state = 0;

    //绑定信号与槽
    connect(ui->pushButton_clean_recv_buff,&QPushButton::clicked,this,&MainWindow::pushButton_clean_recv_buff_cb);
    connect(ui->pushButton_clean_count,&QPushButton::clicked,this,&MainWindow::pushButton_clean_count_cb);
    connect(ui->pushButton_clean_input,&QPushButton::clicked,this,&MainWindow::pushButton_clean_input_cb);
    connect(ui->pushButton_open_uart,&QPushButton::clicked,this,&MainWindow::pushButton_open_uart_cb);
    connect(ui->pushButton_send_data,&QPushButton::clicked,this,&MainWindow::pushButton_send_data_cb);
    connect(serial, &QSerialPort::readyRead, this, &MainWindow::read_data_rb);
}

void MainWindow::pushButton_send_data_cb()
{

    serial->write(ui->textEdit_send->toPlainText().toLatin1());
    if(ui->checkBox_send_linefeed->isChecked()==true)
    {
        serial->write("\n");
    }
    QString send_buff = ui->textEdit_send->toPlainText();
    this->sendByte_count = this->sendByte_count + send_buff.length();
    QString send_count_str = QString::number(this->sendByte_count);
    ui->label_sendbyte_count->setText(send_count_str);
}


void MainWindow::pushButton_open_uart_cb()
{
    qDebug()<<"pushButton_open_uart_cb is running!";
    if(ui->pushButton_open_uart->text()==tr("open"))
    {
        qDebug()<<ui->pushButton_open_uart->text();
        serial = new QSerialPort;
        serial->setPortName("/dev/ttymxc2");
        if(serial->open(QIODevice::ReadWrite))
        {
            qDebug() << "open uart succ";
            //如果串口打开成功，则开始配置串口参数
            serial->setDataBits(QSerialPort::Data8);
            serial->setParity(QSerialPort::NoParity);
            serial->setStopBits(QSerialPort::OneStop);
            serial->setFlowControl(QSerialPort::NoFlowControl);
            serial->setBaudRate(QSerialPort::Baud115200);
            connect(serial, &QSerialPort::readyRead, this, &MainWindow::read_data_rb);
            ui->comboBox_baud->setEnabled(false);
            ui->comboBox_port->setEnabled(false);
            ui->comboBox_databit->setEnabled(false);
            ui->comboBox_stopbit->setEnabled(false);
            ui->comboBox_checkbit->setEnabled(false);
            ui->pushButton_open_uart->setText("close");
            ui->label_state->setStyleSheet(this->state_red_SheetStyle);
        }
    }
    else
    {
        serial->clear();
        serial->close();
        serial->deleteLater();
        qDebug() << "close uart succ";
        ui->comboBox_baud->setEnabled(true);
        ui->comboBox_port->setEnabled(true);
        ui->comboBox_databit->setEnabled(true);
        ui->comboBox_stopbit->setEnabled(true);
        ui->comboBox_checkbit->setEnabled(true);
        ui->pushButton_open_uart->setText("open");
        ui->label_state->setStyleSheet(this->state_grey_SheetStyle);
    }

}

void MainWindow::read_data_rb()
{
    QByteArray buf;
    buf = serial->readAll();
    if(!buf.isEmpty())
    {
        QString str;
        QTextCodec *codec = QTextCodec::codecForName("GBK");//指定QString的编码方式
        QString str_buf=codec->toUnicode(buf);//buf转换成QString类型

        if(ui->radioButton_recv_hex->isChecked() == true)
        {
            buf=buf.toHex();     //或者使用这句话将buf转换成十六进制显示
            qDebug()<<"recv_hex";
            str_buf=tr(buf);
        }


        //ui->textBrowser_recv->clear();

        ui->textBrowser_recv->append(str_buf);
        this->recvByte_count = this->sendByte_count + str_buf.length();
        QString send_count_str = QString::number(this->recvByte_count);
        ui->label_recvbyte_count->setText(send_count_str);
    }
    buf.clear();
}


void MainWindow::pushButton_clean_count_cb()
{
    this->recvByte_count = 0;
    this->sendByte_count = 0;
    ui->label_recvbyte_count->setText("0");
    ui->label_sendbyte_count->setText("0");
}

void MainWindow::pushButton_clean_input_cb()
{
    ui->textEdit_send->clear();
}

void MainWindow::pushButton_clean_recv_buff_cb()
{
    ui->textBrowser_recv->clear();
}

MainWindow::~MainWindow()
{
    delete ui;
}

